from pdf2docx import Converter
import tabula
from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
import pandas



root = Tk()
root.title("File Converter")
root.geometry("300x200")


def pdftodocx():
    old = filedialog.askopenfilename(filetypes=[("PDF Files", "*.pdf")])
    new = filedialog.asksaveasfilename(defaultextension=".docx", filetypes=[("DOCX Files", "*.docx")])
    if old and new:
        obj = Converter(old)
        obj.convert(new)
        obj.close()
        messagebox.showinfo("Information","Completed !!!")


def pdftocsv():
    old = filedialog.askopenfilename(filetypes=[("PDF Files", "*.pdf")])

    new = filedialog.asksaveasfilename(defaultextension=".csv", filetypes=[("Excel Files", "*.csv")])

    if old and new:
        tabula.convert_into(old, new, output_format="csv", pages='all')
        messagebox.showinfo("Information","Completed !!!")
        

convert_button1 = Button(text="Convert PDF to DOCX",height=3, width=30, command=pdftodocx)
convert_button1.place(x=40, y=30)

convert_button2 = Button(text="Convert PDF TO EXCEL",height=3, width=30, command=pdftocsv)
convert_button2.place(x=40, y=100)

root.mainloop()
